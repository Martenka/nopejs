package eu.espeo.demo.basic;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import eu.espeo.demo.basic.exceptions.ExemplaryException;

@Service
public class AppService {
    private final Logger logger = LoggerFactory.getLogger(AppService.class);
    
    public String getHello() {
        logger.debug("getHello()");
        return "Hello World!";
    }

    public List<String> getAll() {
        logger.debug("getAll()");
        return List.of(getHello());
    }

    public void riseException() {
        logger.debug("riseException()");
        throw new ExemplaryException();
    }
}
