package eu.espeo.demo.basic.model;

public enum Title {
    MR("Mr."),
    MRS("Mrs."),
    MS("Ms.");

    private Title(String title) {
        this.title = title;
    }

    private String title;

    @Override
    public String toString() {
        return title;
    }
}
