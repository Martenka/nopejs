package eu.espeo.demo.basic.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import eu.espeo.demo.basic.AppService;

@RestController
public class AppController {
    
    private final AppService appService;
    private final Logger logger = LoggerFactory.getLogger(AppController.class);

    public AppController(AppService appService) {
        this.appService = appService;
    }

    @GetMapping("/{id}")
    public String getHello(@PathVariable String id) {
        logger.info("getHello({})", id);
        return appService.getHello();
    }

    @GetMapping("/")
    public List<String> getAll() {
        logger.info("getAll()");
        return appService.getAll();
    }

    @PostMapping("/greetings")
    public GreetingResponse getGreeting(@RequestBody GreetingRequest request) {
        logger.info("getGreeting({})", request);
        return new GreetingResponse("Hello " + request.title() + " " + request.name());
    }

    @GetMapping("/exceptions/handled")
    public void riseException() {
        logger.info("riseException()");
        appService.riseException();
    }


    @GetMapping("/exceptions/unhandled")
    public void riseUnhandledException() {
        logger.info("riseUnhandledException()");
        throw new RuntimeException("Unexpected");
    }
}
