package eu.espeo.demo.basic.rest;

import eu.espeo.demo.basic.model.Title;

public record GreetingRequest(Title title, String name) {}