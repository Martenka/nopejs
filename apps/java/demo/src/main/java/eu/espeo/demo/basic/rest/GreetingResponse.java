package eu.espeo.demo.basic.rest;

public record GreetingResponse(String message) {}
