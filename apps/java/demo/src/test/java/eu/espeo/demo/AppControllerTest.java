package eu.espeo.demo;

import org.junit.jupiter.api.Test;

import eu.espeo.demo.basic.AppService;
import eu.espeo.demo.basic.rest.AppController;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

class AppControllerTest {

    private final AppController uut = new AppController(new AppService());

    @Test
    void test() {
        var result = uut.getAll();
        System.out.println(result);
        assertThat(result).contains("Hello World!");
    }

}