import { Module } from '@nestjs/common';
import { AppController } from './rest/app.controller';
import { AppService } from './app.service';
import { AppControllerWithoutFilter } from './rest/app-without-handler.controller';

@Module({
  imports: [],
  controllers: [AppController, AppControllerWithoutFilter],
  providers: [AppService],
})
export class AppModule {}
