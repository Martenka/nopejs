import { Injectable, Logger } from '@nestjs/common';
import { ExemplaryException } from './exceptions/exemplary.exception';

@Injectable()
export class AppService {
  private readonly logger = new Logger(AppService.name);

  getHello(): string {
    this.logger.debug('getHello()');
    return 'Hello World!';
  }

  getAll(): string[] {
    this.logger.debug('getAll()');
    return [this.getHello()];
  }

  riseException() {
    this.logger.debug('riseException()');
    throw new ExemplaryException();
  }
}
