import { Controller, Get, Logger } from '@nestjs/common';
import { ExemplaryException } from '../exceptions/exemplary.exception';

@Controller()
export class AppControllerWithoutFilter {
  private readonly logger = new Logger(AppControllerWithoutFilter.name);

  @Get('/no-filter/exceptions')
  riseControllerException(): void {
    this.logger.log('riseControllerException()');
    throw new ExemplaryException();
  }
}
