import {
  Body,
  Controller,
  Get,
  Logger,
  Param,
  Post,
  UseFilters,
} from '@nestjs/common';
import { AppService } from '../app.service';
import { GreetingRequest } from './greeting-request';
import { GreetingResponse } from './greeting-response';
import { AppExceptionFilter } from './app.exception-filter';

@Controller()
@UseFilters(AppExceptionFilter)
export class AppController {
  constructor(private readonly appService: AppService) {}

  private readonly logger = new Logger(AppController.name);

  @Get('/exceptions/handled')
  riseException(): void {
    this.logger.log('riseException()');
    this.appService.riseException();
  }

  @Get('/exceptions/unhandled')
  riseUnhandledException(): void {
    this.logger.log('riseUnhandledException()');
    throw new Error('Unexpected');
  }

  @Get('/:id')
  getHello(@Param('id') id: string): string {
    this.logger.log(`getHello(${id})`);
    return this.appService.getHello();
  }

  @Get('/')
  getAll(): string[] {
    this.logger.log(`getAll()`);
    return this.appService.getAll();
  }

  @Post('/greetings')
  getGreeting(@Body() request: GreetingRequest): GreetingResponse {
    this.logger.log(`getGreeting(${JSON.stringify(request)})`);
    return new GreetingResponse(`Hello ${request.title} ${request.name}`);
  }
}
