import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpStatus,
  Logger,
} from '@nestjs/common';
import { ExemplaryException } from '../exceptions/exemplary.exception';
import { Response } from 'express';

@Catch(ExemplaryException)
export class AppExceptionFilter implements ExceptionFilter<ExemplaryException> {
  private readonly logger = new Logger(AppExceptionFilter.name);

  catch(exception: ExemplaryException, host: ArgumentsHost) {
    this.logger.warn(exception.stack);
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    response.status(HttpStatus.NOT_IMPLEMENTED).send('Exemplary exception');
  }
}
