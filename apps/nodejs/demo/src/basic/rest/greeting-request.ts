import { Title } from '../model/title.enum';

export class GreetingRequest {
  title: Title;
  name: string;
}
