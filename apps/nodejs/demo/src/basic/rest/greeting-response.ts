export class GreetingResponse {
  message: string;

  constructor(_message: string) {
    this.message = _message;
  }
}
